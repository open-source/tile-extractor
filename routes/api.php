<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/extract/{west}/{east}/{south}/{north}', 'TileRequestController@get_all_tile_data_in_bounds');
Route::middleware('cors')->get('/extract-amount/{west}/{east}/{south}/{north}', 'TileRequestController@get_tile_amount_in_bounds');
Route::middleware('cors')->get('/extract-size/{west}/{east}/{south}/{north}', 'TileRequestController@get_tile_size_in_bounds');
