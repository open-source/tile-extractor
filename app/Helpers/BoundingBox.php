<?php

namespace App\Helpers;

class BoundingBox
{
    private $zoom_level;

    public $west_bound;
    public $east_bound;
    public $north_bound;
    public $south_bound;

    public $west_tile_bound;
    public $east_tile_bound;
    public $north_tile_bound;
    public $south_tile_bound;

    public function __construct($zoom_level, $west_bound, $east_bound, $south_bound, $north_bound)
    {
        $this->zoom_level = $zoom_level;

        $this->west_bound = $west_bound;
        $this->east_bound = $east_bound;
        $this->south_bound = $south_bound;
        $this->north_bound = $north_bound;

        $this->west_tile_bound = $this->lonZoomTotile($west_bound, $zoom_level);
        $this->east_tile_bound = $this->lonZoomTotile($east_bound, $zoom_level);
        $this->south_tile_bound = $this->latZoomTotile($south_bound, $zoom_level);
        $this->north_tile_bound = $this->latZoomTotile($north_bound, $zoom_level);
    }

    public function flip()
    {
        $this->south_tile_bound = $this->flip_row($this->zoom_level, $this->south_tile_bound);
        $this->north_tile_bound = $this->flip_row($this->zoom_level, $this->north_tile_bound);
    }

    public static function flip_row($zoom, $row)
    {
        return pow(2, $zoom) - 1 - $row;
    }

    private function lonZoomTotile($lon, $zoom)
    {
        return floor((($lon + 180) / 360) * pow(2, $zoom));
    }

    private   function latZoomTotile($lat, $zoom)
    {
        return floor(
            ((1 -
                log(
                    tan(($lat * M_PI) / 180) + 1 / cos(($lat * M_PI) / 180)
                ) /
                M_PI) /
                2) *
                pow(2, $zoom)
        );
    }
}
