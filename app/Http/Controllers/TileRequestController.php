<?php

namespace App\Http\Controllers;

use App\Helpers\BoundingBox;
use App\Models\Tile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

class TileRequestController extends Controller
{
    public static function get_all_tile_data_in_bounds(Request $request, $west_bound, $east_bound, $south_bound, $north_bound)
    {
        if ($east_bound < $west_bound || $north_bound < $south_bound || $east_bound - $west_bound > 4 || $north_bound - $south_bound > 2.5) {
            abort(400);
        }

        return response()->stream(function () use ($west_bound, $east_bound, $south_bound, $north_bound, $request) {
            TileRequestController::stream_all_tile_data_in_bounds($west_bound, $east_bound, $south_bound, $north_bound);
        });
    }

    private static function stream_all_tile_data_in_bounds($west_bound, $east_bound, $south_bound, $north_bound)
    {

        $options = new Archive();
        $options->setSendHttpHeaders(true);
        $options->setFlushOutput(true);

        $zip = new ZipStream(time() . ".zip", $options);

        for ($zoom_level = 1; $zoom_level <= 14; $zoom_level++) {
            $bounding_box = new BoundingBox($zoom_level, $west_bound, $east_bound, $north_bound, $south_bound);
            $bounding_box->flip();

            $base_tile_request = Tile::where('zoom_level', '=', $zoom_level)
                ->where('tile_column', '>=', $bounding_box->west_tile_bound)
                ->where('tile_column', '<=', $bounding_box->east_tile_bound)
                ->where('tile_row', '>=', $bounding_box->north_tile_bound)
                ->where('tile_row', '<=', $bounding_box->south_tile_bound);
            $sorted_tile_request = $base_tile_request
                ->orderBy('zoom_level')
                ->orderby('tile_column')
                ->orderBy('tile_row');
            foreach ($sorted_tile_request->cursor() as $tile) {
                // The download can take a while
                // Reset the time limit
                set_time_limit(30);
                Log::debug("Serving tile " . $tile);
                $fp = tmpfile();
                fwrite($fp, $tile->tile_data);
                rewind($fp);
                $zip->addFileFromStream($zoom_level . "/" . $tile->tile_column . "/" . $tile->tile_row . ".mbtile", $fp);

            }
        }
        $zip->finish();
    }

    public function get_tile_amount_in_bounds(Request $request, $west_bound, $east_bound, $south_bound, $north_bound)
    {
        if ($east_bound < $west_bound || $north_bound < $south_bound || $east_bound - $west_bound > 4 || $north_bound - $south_bound > 2.5) {
            abort(400);
        }

        $total_tile_amount = 0;

        for ($zoom_level = 1; $zoom_level <= 14/*env('MAX_ZOOM_LEVEL')*/; $zoom_level++) {
            $bounding_box = new BoundingBox($zoom_level, $west_bound, $east_bound, $north_bound, $south_bound);
            $bounding_box->flip();

            $base_tile_request = Tile::where('zoom_level', '=', $zoom_level)
                ->where('tile_column', '>=', $bounding_box->west_tile_bound)
                ->where('tile_column', '<=', $bounding_box->east_tile_bound)
                ->where('tile_row', '>=', $bounding_box->north_tile_bound)
                ->where('tile_row', '<=', $bounding_box->south_tile_bound);
            $tile_count = $base_tile_request
                ->count();
            $total_tile_amount += $tile_count;
        }

        echo $total_tile_amount;
    }

    public function get_tile_size_in_bounds(Request $request, $west_bound, $east_bound, $south_bound, $north_bound)
    {
        if ($east_bound < $west_bound || $north_bound < $south_bound || $east_bound - $west_bound > 4 || $north_bound - $south_bound > 2.5) {
            abort(400);
        }

        $total_tile_size = 0;

        for ($zoom_level = 1; $zoom_level <= 14/*env('MAX_ZOOM_LEVEL')*/; $zoom_level++) {
            $bounding_box = new BoundingBox($zoom_level, $west_bound, $east_bound, $north_bound, $south_bound);
            $bounding_box->flip();

            $base_tile_request = Tile::where('zoom_level', '=', $zoom_level)
                ->where('tile_column', '>=', $bounding_box->west_tile_bound)
                ->where('tile_column', '<=', $bounding_box->east_tile_bound)
                ->where('tile_row', '>=', $bounding_box->north_tile_bound)
                ->where('tile_row', '<=', $bounding_box->south_tile_bound);
            $tile_size = $base_tile_request
                ->sum(DB::raw('length(tile_data)'));
            $total_tile_size += $tile_size;
        }

        echo $total_tile_size;
    }
}
