<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tile extends Model
{
    protected $connection = 'map';
    protected $table = 'tiles';
    protected $primaryKey = 'rowid';

    public function serialize(): String
    {
        $zoom_level_bytes = $this->pack_int32be($this->zoom_level); // Big-Endian 32 Bit
        $tile_col_bytes = $this->pack_int32be($this->tile_column); // Big-Endian 32 Bit
        $tile_row_bytes = $this->pack_int32be($this->tile_row); // Big-Endian 32 Bit
        $tile_data_size = strlen($this->tile_data);
        $tile_data_size_bytes = $this->pack_int32be($tile_data_size); // Big-Endian 32 Bit
        $tile_data_bytes = $this->tile_data; // Raw direct Bytes
        return implode("", [$zoom_level_bytes, $tile_col_bytes, $tile_row_bytes, $tile_data_size_bytes, $tile_data_bytes]);
    }

    private function pack_int32be($i)
    {
        if ($i < -2147483648 || $i > 2147483647) {
            die("Out of bounds");
        }
        return pack('C4',
            ($i >> 24) & 0xFF,
            ($i >> 16) & 0xFF,
            ($i >> 8) & 0xFF,
            ($i >> 0) & 0xFF
        );

    }

    public function __toString()
    {
        return sprintf("(%d)[%d, %d]-(%d)", $this->zoom_level, $this->tile_column, $this->tile_row, strlen($this->tile_data));
    }
}
